#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include "myfunction.h"

#define MAX_BUF_SIZE 1024
#define MAX_ADDRESS_LEN 46 						/*this macro is used to represent the Max length of address */

int main(int argc, char *argv[]){
	struct sockaddr_in server_addr;
	struct sockaddr_in client_addr;
	int serverPort;								/*new variable used to take in input a port*/
	char IP_Address[MAX_ADDRESS_LEN]; 			/*new variable used to take in inpit a IP address*/
	int sfd;
	int br;
	int i;
	int space = 0;
	int stop = 0;
	ssize_t byteRecv;
	ssize_t byteSent;
	size_t msgLen;
	socklen_t serv_size;
	char receivedData [MAX_BUF_SIZE];
	char sendData [MAX_BUF_SIZE];
	sfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	
	if (sfd < 0){
	perror("socket");
	exit(EXIT_FAILURE);
	}
	
	printf("Please insert the server port: ");
	scanf("%d",&serverPort);
	
	printf("\nPlease insert IP address: ");
	scanf("%s",IP_Address);
	
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(serverPort);
	server_addr.sin_addr.s_addr = inet_addr(IP_Address);
	serv_size = sizeof(server_addr);
	
	while(!stop){
		printf("Please send a message to send to the server: \n");
		if(space == 0){
			fgetc(stdin);
			space = 1;
		}
		fgets(sendData,1024,stdin);
		printf("String going to be sent to server: %s\n", sendData);
		msgLen = countStrLen(sendData);
		byteSent = sendto(sfd, sendData, msgLen, 0, (struct sockaddr *)
		&server_addr, sizeof(server_addr));
		printf("Bytes sent to server: %zd\n", byteSent);
		if(!stop){
			byteRecv = recvfrom(sfd, receivedData, MAX_BUF_SIZE, 0, (struct
			sockaddr *) &server_addr, &serv_size);
			printf("Received from server: ");
			printData(receivedData, byteRecv);
			printf("STRINGA : %s ",sendData);
			if(strncmp(sendData, "exit", 4) == 0){
				if(strncmp(receivedData, "goodbye", 7) == 0){
				stop = 1;
				}
			}
		}
	}
	return 0;
}
