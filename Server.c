#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include "myfunction.h"

#define MAX_BUF_SIZE 1024 
#define SERVER_PORT 9876
#define MAX_ADDRESS_LEN 46 						/*this macro is used to represent the Max length of address */

int main(int argc, char *argv[]){
	
	struct sockaddr_in server_addr;
	struct sockaddr_in client_addr;
	int sfd;
	int br;
	int serverPort;								/*new variable used to take in input a port*/
	char IP_Address[MAX_ADDRESS_LEN]; 			/*new variable used to take in inpit a IP address*/
	char temp[MAX_BUF_SIZE];
	ssize_t byteRecv;
	ssize_t byteSent;
	socklen_t cli_size;
	char receivedData [MAX_BUF_SIZE];
	/*char sendData[MAX_BUF_SIZE];*/
	sfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	
	if (sfd < 0){
		perror("socket");
		exit(EXIT_FAILURE);
	}
	
	printf("Please insert the server port: ");
	scanf("%d",&serverPort);
	
	printf("\nPlease insert IP address: ");
	scanf("%s",IP_Address);
	
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(serverPort);
	server_addr.sin_addr.s_addr = inet_addr(IP_Address);
	br = bind(sfd, (struct sockaddr *) &server_addr, sizeof(server_addr));
	if (br < 0){
		perror("bind");
		exit(EXIT_FAILURE);
	}
	cli_size = sizeof(client_addr);
	
	for(;;){
		byteRecv = recvfrom(sfd, receivedData, MAX_BUF_SIZE, 0, (struct
		sockaddr *) &client_addr, &cli_size);
		if(byteRecv == -1){
			perror("recvfrom");
			exit(EXIT_FAILURE);
		}
		
		printf("\nReceived from client %s:%d",inet_ntoa(client_addr.sin_addr),ntohs(client_addr.sin_port));
		printf("\nReceived data: ");
		printData(receivedData, byteRecv);
		if(strncmp(receivedData, "exit", byteRecv - 1) == 0){
			strcpy(temp,"goodbye");
			printData(temp,8);
			byteSent = sendto(sfd, temp, 8, 0, (struct sockaddr *) &client_addr, sizeof(client_addr));
		}else{
			printf("Response to be sent back to client: ");
			printData(receivedData, byteRecv);
			byteSent = sendto(sfd, receivedData, byteRecv, 0, (struct sockaddr *) &client_addr, sizeof(client_addr));
		}
	}
	return 0;
}
